const express = require('express');
const app = express();
var cors = require('cors');
const pg = require('pg');
const querystring = require('querystring');
app.use(cors());

const config = {
    host: 'localhost',
    // Do not hard code your username and password.
    // Consider using Node environment variables.
    user: 'node',     
    password: 'node',
    database: 'DatenbankProjekt',
    port: 5432,
    ssl: false
};
const client = new pg.Client(config);

app.options('/api/getalldata', cors()) // enable pre-flight request for DELETE request
app.options('/api/getcountrydata',cors())
app.options('/api/getcountrylist',cors())



async function queryCountryList()
{
    let qres = null;
    
    //Using the query method, the postgres wrapper automatically checks the parameters for sql injections
    await client.query('SELECT DISTINCT code, name AS entity FROM countries ORDER BY entity\;').then( (results) => 
    {

         qres = results.rows;
        
    } )
    
    return qres;
    
}

async function queryHappiness(countryCodes)
{
    let qres = null;
    
    //Using the query method, the postgres wrapper automatically checks the parameters for sql injections
    
    // ACHTUNG: MUSS NOCH EFFIZIENT GEMACHT WERDEN

    //Ergänzung der Datenbank durch
    //INSERT INTO happiness (SELECT year, 'average', '', 0, AVG(happinessscore) AS happinessscore FROM happiness GROUP BY year);
    await client.query('(SELECT DISTINCT happiness.year, happiness.code, happinessscore FROM  happiness  WHERE happiness.code=$1 ORDER BY happiness.year)  \;', countryCodes).then( (results) => 
    {
        qres = results.rows;
        
    } )
    
    return qres;
}

async function queryCO2(countryCodes)
{
    let qres = null;
    

    //                    entity                    |   code   | year |   annualemission  
    //Using the query method, the postgres wrapper automatically checks the parameters for sql injections
   // SELECT * FROM co2emissions WHERE code='avg';
    // ACHTUNG: MUSS NOCH EFFIZIENT GEMACHT WERDEN
    await client.query('SELECT  co2emissions.code, year, annualemission / \"Count\" AS emissionpercapita FROM  (co2emissions INNER JOIN population ON  co2emissions.code = population.code AND co2emissions.year = \"population\".\"Year\") WHERE co2emissions.code=$1 ORDER BY year\;', countryCodes).then( (results) => 
    {
        qres = results.rows;
        
    } )
    
    return qres;
}

async function queryGDP(countryCodes)
{
    let qres = null;
    
    //Using the query method, the postgres wrapper automatically checks the parameters for sql injections
    console.log("Query gdp")
    // ACHTUNG: MUSS NOCH EFFIZIENT GEMACHT WERDEN
    //
    let query = 'SELECT gdp.year, gdp.countrycode AS code, gdp.gdp / population."Count" AS gdp_percapita FROM (gdp INNER JOIN population ON gdp.year = population."Year" AND (population.code = gdp.countrycode)) WHERE gdp.countrycode = $1 AND gdp.year > 2009  ORDER BY gdp.year\;'
    //let query = 'SELECT gdp.year, countrycode AS code, gdp / population."Count" AS gdp_percapita FROM (gdp INNER JOIN population ON (gdp.year = population."Year" AND gdp.countryname = \' \' || population."Country")) WHERE countrycode = $1 AND gdp.year > 2009 ORDER BY gdp.year\;'
    //Due to an error in the data conversion, the GDP countryname always contains an extra space in the beginning; thus we work around this by the above string concat
    // SELECT gdp.year, countries.code AS code, gdp/ population."Count" FROM ( (countries INNER JOIN gdp ON gdp.countryname = ' ' || countries.name or gdp.countryname = ' ' || countries.name2) INNER JOIN population ON population."Year" = gdp.year AND population."Country" = countries.name);
    
    await client.query(query, countryCodes).then( (results) => 
    {   
        qres = results.rows;
        
    } )
    

    return qres;
    
}


async function queryCovariance(countryCodes)
{
    var averages = null;
    
    //Using the query method, the postgres wrapper automatically checks the parameters for sql injections
    
    // ACHTUNG: MUSS NOCH EFFIZIENT GEMACHT WERDEN

    //Ergänzung der Datenbank durch
    //INSERT INTO happiness (SELECT year, 'average', '', 0, AVG(happinessscore) AS happinessscore FROM happiness GROUP BY year);
    await client.query('SELECT DISTINCT AVG(happinessscore) as happinessscore, AVG(gdp.gdp) / AVG(population."Count") as gdp, AVG(annualemission / population."Count") AS emissionpercapita FROM  happiness INNER JOIN gdp ON gdp.year = happiness.year and gdp.countrycode = happiness.code INNER JOIN co2emissions ON co2emissions.code = happiness.code and co2emissions.year = happiness.year INNER JOIN population ON population."Year" = happiness.year and population.code = happiness.code  WHERE happiness.year > 2014 AND happiness.code = $1\;', countryCodes).then( (results) => 
    {
        averages = results.rows;
        
    } )
    console.log("loaded average");
    happiness_data = await queryHappiness(countryCodes);
    co2_data = await(queryCO2(countryCodes));
    console.log("loaded co2");
    co2_data = co2_data.filter(x => x.year >= 2015);

    gdp_data = await(queryGDP(countryCodes));
    console.log("loaded gdp");
    gdp_data = gdp_data.filter(x => x.year >= 2015);

    console.trace(co2_data);
    console.trace(averages);
    console.trace(gdp_data);
    //Now we calculate the corvariance between happiness and co2:
    var covariance_happiness_co2 = 0
    for(let v = 0; v < averages.length; v++)
    {
        try {
        co2_data[v].emissionpercapita 
           covariance_happiness_co2 += (new Number(co2_data[v].emissionpercapita - averages[v].emissionpercapita).toPrecision(5)) * (new Number(happiness_data[v].happinessscore - averages[v].happinessscore).toPrecision(5))

        } catch (error) {
            console.trace(error);
        }

    }
    covariance_happiness_co2 /= averages.length
   
    //and now between gdp and happiness
    var covariance_happiness_gdp = 0
    for(let v = 0; v < averages.length; v++)
    {
        console.trace("loop: " + covariance_happiness_gdp)
        try {
           
           covariance_happiness_gdp += (new Number(gdp_data[v].gdp_percapita - averages[v].gdp).toPrecision(10)) * (new Number(happiness_data[v].happinessscore - averages[v].happinessscore).toPrecision(10))
           
           console.log("Zwischenergebnis:" + covariance_happiness_gdp);
        } catch (error) {
            
            console.log(error);
        }

    }
    covariance_happiness_gdp /= averages.length

    return {'covariance_happiness_co2' : covariance_happiness_co2, 'covariance_happiness_gdp' : covariance_happiness_gdp };
}

client.connect(err => {
    if (err) throw err;
    else { return; }
});


app.options('*', cors());
app.route('/api/getco2').get(async(req, res) => {
    const rows = await queryCO2(req.query.countryList.split(","));
    //res.setHeader('Access-Control-Allow-Origin', 'http://localhost')
    res.send(rows);
      
  }, cors())

  app.route('/api/getgdp').get(async(req, res) => {
    if(req.query.countryList!=undefined){
    const rows = await queryGDP(req.query.countryList.split(","));
    //res.setHeader('Access-Control-Allow-Origin', 'http://localhost')
    res.send(rows);
    }else
    {
        res.send(null);
    }
      
  }, cors())

app.route('/api/getcountrydata').get(async(req, res) => {
    if(req.query.countryList!=undefined){
        const rows = await queryCountryData(req.query.countryList.split(","));
        console.log("Valid query");
    //res.setHeader('Access-Control-Allow-Origin', 'http://localhost')
         res.send(rows); return;}
    res.send(null);
      
  }, cors())

  app.route('/api/getcountrylist').get(async(req, res) => {
    const rows = await queryCountryList();
    //res.setHeader('Access-Control-Allow-Origin', 'http://localhost')
    res.send(rows);
      
  }, cors())

  app.route('/api/gethappiness').get(async(req, res) => {
    if(req.query.countryList!=undefined){
        const rows = await queryHappiness(req.query.countryList.split(","));
        console.log("Valid query");
    //res.setHeader('Access-Control-Allow-Origin', 'http://localhost')
         res.send(rows); return;}
    res.send(null);
      
  }, cors())

  app.route('/api/getcovariance').get(async(req, res) => {
    if(req.query.countryList!=undefined){
        const rows = await queryCovariance(req.query.countryList.split(","));
        console.log("Valid query");
    //res.setHeader('Access-Control-Allow-Origin', 'http://localhost')
         res.send(rows); return;}
    res.send(null);
      
  }, cors())

  app.listen(8000, () => {
    console.log('Server started!')
  })

  