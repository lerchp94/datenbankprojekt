//Datenbanksysteme
// Janka Bothe, Philipp Lerch
//Übung 9
//Laufzeitumgebung: node.js
//Dieses Skript implementiert zwei RESTful Methoden, welche die Anforderungen aus dem Übungsblatt erfüllt.
//In der PG-Datenbank befindet sich eine Tabelle "Ue09" mit den zwei Spalten "a" und "b"


const express = require('express');
const app = express();
var cors = require('cors');
const pg = require('pg');
const querystring = require('querystring');
app.use(cors());

const config = {
    host: 'localhost',
    // Do not hard code your username and password.
    // Consider using Node environment variables.
    user: 'node',     
    password: 'node',
    database: 'DatenbankProjekt',
    port: 5432,
    ssl: false
};
const client = new pg.Client(config);

app.options('/api/query', cors()) // enable pre-flight request for DELETE request

async function query()
{
    let qres = null;
    await client.query('SELECT * FROM ue09\;').then( (results) => 
    {
         qres = results.rows;
        
    } )
   // console.log("Done with query %s", String(qres));
    return qres;
    
}
async function queryupdate()
{
    let qres = null;
    await client.query('INSERT INTO ue09 (a,b) VALUES($1, $2)\;', [new Date().toTimeString(), Math.random().toString()]);
    await client.query('SELECT * FROM ue09\;').then( (results) => 
    {
         qres = results.rows;
        
    } )
   // console.log("Done with query %s", String(qres));
    return qres;
    
}



client.connect(err => {
    if (err) throw err;
    else { return; }
});


app.options('*', cors());
app.route('/api/query').get(async(req, res) => {
    const rows = await query();
    //res.setHeader('Access-Control-Allow-Origin', 'http://localhost')
    res.send(rows);
      
  }, cors())
  app.route('/api/queryupdate').get(async(req, res) => {
    const rows = await queryupdate();
    //res.setHeader('Access-Control-Allow-Origin', 'http://localhost')
    res.send(rows);
      
  }, cors())

  app.listen(8000, () => {
    console.log('Server started!')
  })

  