import { Component, OnInit } from '@angular/core';
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import { catchError, retry } from 'rxjs/operators';
//import { DataColumn } from './datacolumn';
import {DataService} from './app.data.service';
import { Country} from './country'

import svgMap from 'svgmap';
import 'svgmap/dist/svgMap.min.css';
import { CO2Row, GDPRow, PopulationRow, HappinessRow, CovarianceRow } from './datacolumn';
@Injectable()
export class ConfigService {
  constructor(private http: HttpClient) { }
}

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  title = 'Visualisierung';

  co2data: Array<CO2Row> = [];
  gdpdata: Array<GDPRow> = [];
  covarianceData: CovarianceRow = {covariance_happiness_co2: 0, covariance_happiness_gdp:0};
  popdata: Array<PopulationRow> = [];
  happinessdata: Array<HappinessRow> = [];
  private countries_: Array<Country> = [];
  private selectedCountries_: Array<Country> = [];
  public get countries(): Array<Country> {
    return this.countries_;
  }
  public set countries(value: Array<Country>) {
    this.countries_ = value;
  }

  public get selectedCountries(): Array<Country> {
    return this.selectedCountries_;
  }
  public set selectedCountries(value: Array<Country>) {
    this.selectedCountries_ = value;
  }
  constructor(private dataService: DataService) { }
  ngOnInit() {
    let countryList = ["DEU"];
    this.getAllData(countryList);
  }

  
    
  getAllData(country): void {
    let countryList = country;
    this.getGDPData(countryList);
    
   //this.getPopData(countryList);
    this.getHappinessData(countryList);
    this.getCO2Data(countryList);
    console.log(countryList);
    this.getCountryList();
    this.getCovarianceData(countryList);
  }

  
  getGDPData(country): void {
    this.dataService.getGDP([country])
        .subscribe(data => { this.gdpdata = data});
  }
  getCO2Data(country): void {
    this.dataService.getCO2([country])
        .subscribe(data => { this.co2data = data.filter(a => a.year >= 2010 )});
  }
  getPopData(country): void {
    this.dataService.getPopulation([country])
        .subscribe(data => { this.popdata = data});
  }
  getHappinessData(country): void {
    this.dataService.getHappiness(country)
        .subscribe(data => { this.happinessdata = data});
  }
  getCovarianceData(country): void {
    this.dataService.getCovariance(country)
        .subscribe(data => { console.trace(data); this.covarianceData= data});
  }


  getCountryList(): void {
    this.dataService.getCountryList()
        .subscribe(data => { this.countries = data});
  }
  get dataSourceHappiness(): Array<HappinessRow> { return this.happinessdata; }
  get dataSourcePopulation(): Array<PopulationRow> { return this.popdata; }
  get dataSourceGDP(): Array<GDPRow> { return this.gdpdata; }
  get dataSourceCovariance(): CovarianceRow { return this.covarianceData; }
  get dataSourceCO2(): Array<CO2Row> { return this.co2data; }
}
