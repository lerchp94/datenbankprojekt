import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import { Injectable } from '@angular/core';
import {HappinessRow, CountryRow, GDPRow, PopulationRow, CO2Row, CovarianceRow} from './datacolumn';

import {Country} from './country'
//Provides an adapter to the http rest service
@Injectable({
    providedIn: 'root',
  })
export class DataService {
constructor(
    private http: HttpClient){}
    url_gethappiness = 'http://localhost:8000/api/gethappiness';  // URL to web api
    url_getco2 = 'http://localhost:8000/api/getco2';  // URL to web api
    url_getpopulation= 'http://localhost:8000/api/getpopulation';  // URL to web api
    url_getgdp= 'http://localhost:8000/api/getgdp';  // URL to web api
    url_getcountrylist= 'http://localhost:8000/api/getcountrylist';  // URL to web api
    url_getcovariance = 'http://localhost:8000/api/getcovariance'
    
getHappiness(countries : String[]): Observable<HappinessRow[]> {
        let params = new HttpParams();
        params = params.append('countryList', countries.join(","));
        return this.http.get<HappinessRow[]>(this.url_gethappiness, { params: params });
      }
getGDP(countries : String[]): Observable<GDPRow[]> {
        let params = new HttpParams();
        params = params.append('countryList', countries.join(","));
        return this.http.get<GDPRow[]>(this.url_getgdp, { params: params });
      }
getPopulation(countries : String[]): Observable<PopulationRow[]> {
        let params = new HttpParams();
        params = params.append('countryList', countries.join(","));
        return this.http.get<PopulationRow[]>(this.url_getpopulation, { params: params });
      }
getCO2(countries : String[]): Observable<CO2Row[]> {
        let params = new HttpParams();
        params = params.append('countryList', countries.join(","));
        return this.http.get<CO2Row[]>(this.url_getco2, { params: params });
      }
getCovariance(countries : String[]): Observable<CovarianceRow> {
        let params = new HttpParams();
        params = params.append('countryList', countries.join(","));
        return this.http.get<CovarianceRow>(this.url_getcovariance, { params: params });
     
      }
  getCountryList( ): Observable<Country[]> {
        return this.http.get<Country[]>('http://localhost:8000/api/getcountrylist');
      }
}
