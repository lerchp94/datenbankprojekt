export interface HappinessRow
    {
    //respective method: 
    Code: string;
    year: Number;
    happinessscore : number;
}

export interface GDPRow
{
    year: Number;
    code: string;
    gdp_percapita: number;

}

export interface PopulationRow
{
    code: string;
    year: number;
    population: number;
}

export interface CO2Row
{
    code: string;
    year: number;
    emissionpercapita: number;
}

export interface CountryRow
{
    code: string;
    country: string;
}

export interface CovarianceRow
{
    covariance_happiness_co2: number;
    covariance_happiness_gdp: number;
}