import { isNull } from '@angular/compiler/src/output/output_ast';
import { Component, OnInit, Input } from '@angular/core';
import * as d3 from 'd3'
import { count, isEmpty } from 'rxjs/operators';
import { HappinessRow,CO2Row, GDPRow, CovarianceRow } from '../datacolumn';
import {Country} from '../country'

@Component({
  selector: 'app-scatter',
  templateUrl: './scatter.component.html',
  styleUrls: ['./scatter.component.css']
})
export class ScatterComponent implements OnInit {
  private dataHappiness_ : Array<HappinessRow> = []; 
  private dataCO2_ : Array<CO2Row> = []; 
  private dataGDP_ : Array<GDPRow> = []; 
  constructor() { }
  

  private svg;
  private margin = 50;
  private width = 750 - (this.margin * 2);
  private height = 400 - (this.margin * 2);
  private firstRender : Boolean = false;
  private co2checked_ : Boolean = true;
  private happinesschecked_ : Boolean = true;
  private gdpchecked_ : Boolean = true;

  private countries_: Array<Country> = [];
  @Input()
  public get countries(): Array<Country> {
    return this.countries_;
  }
  public set countries(value: Array<Country>) {
    this.countries_ = value;
  }
  public get country():String{
    if(!this.countries.length )
    {
      return "n/a";

    }else 
    {
      console.trace(this.countries[0].code);
      return this.countries[0].entity;}
  }
 
  // if(this.countries_.length == 0 && this.dataHappiness_ != undefined ) return this.dataHappiness_.filter(d => (this.countries_[0].code == d.Code)); else return []; }
  private covariance: CovarianceRow = {covariance_happiness_co2: 0, covariance_happiness_gdp:0};
  @Input()
  get dataCovariance(): CovarianceRow { return this.covariance;}
   
  set dataCovariance(d: CovarianceRow) { 
    this.covariance = d;
  }
  get dataCovarianceCO2(): Number{ return this.dataCovariance.covariance_happiness_co2}
  get dataCovarianceGDP(): Number{ return this.dataCovariance.covariance_happiness_gdp}
  @Input()
  get dataHappiness(): Array<HappinessRow> { return this.dataHappiness_;}
   
  set dataHappiness(d: Array<HappinessRow>) { 
    this.dataHappiness_ = d;
    this.draw();
  }

  @Input()
  get dataGDP(): Array<GDPRow> { return this.dataGDP_; }
  set dataGDP(d: Array<GDPRow>) { this.dataGDP_ = d;
    this.draw();
  }


  @Input()
  get dataCO2(): Array<CO2Row> { return this.dataCO2_; }
  set dataCO2(d: Array<CO2Row>) { this.dataCO2_ = d;
    this.draw();
  }

  get co2checked(): Boolean { return this.co2checked_ }
  set co2checked(d: Boolean) { this.co2checked_ = d;
    console.log("Check changed");
    this.draw();
  }

  get happinesschecked(): Boolean { return this.happinesschecked_ }
  set happinesschecked(d: Boolean) { this.happinesschecked_ = d;

    this.draw();
  }

  get gdpchecked(): Boolean { return this.gdpchecked_ }
  set gdpchecked(d: Boolean) { this.gdpchecked_ = d;

    this.draw();
  }

  ngOnInit(): void {
     //this.createSvg();
  }
  /*
  public getMaxData(): Number {
   return (Math.max.apply(Math, this.data.map(function(o) { return +o.Year; })))
  }*/

private createSvg(): void {
    this.svg = d3.select("figure#scatter")
    .append("svg")
    .attr("width", this.width + (this.margin * 3))
    .attr("height", this.height + (this.margin * 3))
    .append("g")
    .attr("transform", "translate(" + this.margin + "," + this.margin + ")");
}
/*
private drawEmission(): void {
  // Add X axis
  const x = d3.scaleLinear()
  .domain([2010, 2020])
  .range([ 0, this.width ]);
  this.svg.append("g")
  .attr("transform", "translate(0," + this.height + ")")
  .call(d3.axisBottom(x).tickFormat(d3.format("d")));

  // Add Y axis
  const y = d3.scaleLinear()
  .domain([0, 50])
  .range([ this.height, 0]);
  this.svg.append("g")
  .call(d3.axisLeft(y));


 
  // Add dots
  const dots = this.svg.append('g');
  dots.selectAll("dot")
  .data(this.data)
  .enter()
  .append("circle")
  .attr("cx", d => x(d.year))
  .attr("cy", d => y(+d.perCapitaEmission()))
  .attr("r", 5)
  .style("opacity", .5)
  .style("fill", "#15ff00"); //Grün: Emission


} */


private draw(): void {
  console.log("redraw");
  if(this.firstRender)
  d3.select("figure#scatter").select("svg").remove();
  this.firstRender = true;
  this.createSvg();
  if(this.happinesschecked)
    this.drawHappiness();
  if(this.co2checked)
    this.drawCO2();
  if(this.gdpchecked)
  {
   
    this.drawGDP();
  }
}


private drawGDP(): void {

  // Add X axis
  const x = d3.scaleLinear()
  .domain([2010, 2020])
  .range([ 0, this.width ]);
  


  // Add Y axis
  
  const y = d3.scaleLinear()
  .domain([d3.min(this.dataGDP_, d => d.gdp_percapita) as number, d3.max(this.dataGDP_, d => d.gdp_percapita) as number])
  .range([ this.height, 0]);
  this.svg.append("g")
  .call(d3.axisRight(y)).attr("transform", "translate(" + (this.width + 40) + ")")
  .attr("color", "red");
  /*
  this.svg.append("g")
  .call(d3.axisRight(y)); */

  // Add dots
  const dots = this.svg.append('g');
  dots.selectAll("dot")
  .data(this.dataGDP)
  .enter()
  .append("circle")
  .attr("cx", d => x(d.year))
  .attr("cy", d => y(d.gdp_percapita))
  .attr("r", 5) //radius
  .style("opacity", .5)
  .style("fill","orangered" );

  this.svg.append("path")
  .datum(this.dataGDP)
  .attr("fill", "none")
  .attr("stroke", "orangered")
  .attr("stroke-width", 1.5)
  .attr("d", d3.line()
    .x(function(d) { return x(d['year']) })
    .y(function(d) { return y(d['gdp_percapita']) })
    )
  /*
  //Add labels
  dots.selectAll("text")
  .data(this.data)
  .enter()
  .attr("x", d => x(d.year))
  .attr("y", d => y(d.happinessscore))*/
}


private drawHappiness(): void {
  // Add X axis
  //const filtre = function(d, countries) : Array<HappinessRow> {return d.filter( a => a.Code == countries[0].code) }
  const filtre = function(d,countries) : Array<HappinessRow> {return d};

  const x = d3.scaleLinear()
  .domain([2010, 2020])
  .range([ 0, this.width ]);
  this.svg.append("g")
  .attr("transform", "translate(0," + this.height + ")")
  .call(d3.axisBottom(x).tickFormat(d3.format("d")));

  // Add Y axis
  const y = d3.scaleLinear()
  //.domain([d3.min(filtre(this.dataHappiness, this.countries), d => d.happinessscore) as number, d3.max(filtre(this.dataHappiness, this.countries), d => d.happinessscore) as number])
  .domain([0, 10])
  .range([ this.height, 0]);
  this.svg.append("g")
  .call(d3.axisRight(y))
  .attr("color", "blue");

  // Add dots
  const dots = this.svg.append('g');
  dots.selectAll("dot")
  .data(filtre(this.dataHappiness, this.countries))
  .enter()
  .append("circle")
  .attr("cx", d => x(d.year))
  .attr("cy", d => y(d.happinessscore))
  .attr("r", 5)
  .style("opacity", .5)
  .style("fill","blue" );

  this.svg.append("path")
  .datum(filtre(this.dataHappiness, this.countries))
  .attr("fill", "none")
  .attr("stroke", "blue")
  .attr("stroke-width", 1.5)
  .attr("d", d3.line()
    .x(function(d) { return x(d['year']) })
    .y(function(d) { return y(d['happinessscore']) })
    )
  /*
  //Add labels
  dots.selectAll("text")
  .data(this.data)
  .enter()
  .attr("x", d => x(d.year))
  .attr("y", d => y(d.happinessscore))*/
}
private drawCO2(): void {
  // Add X axis
  //console.log(this.dataCO2)
  console.log(this.calcCovariance(this.dataCO2, this.dataHappiness))
  const x = d3.scaleLinear()
  .domain([2010, 2020])
  .range([ 0, this.width ]);
  this.svg.append("g")
  .attr("transform", "translate(0," + this.height + ")")
  .call(d3.axisBottom(x).tickFormat(d3.format("d")));

  // Add Y axis
  const y = d3.scaleLinear()
  .domain([d3.min(this.dataCO2, d => d.emissionpercapita) as number, d3.max(this.dataCO2, d => d.emissionpercapita) as number])
  .range([ this.height, 0]);
  this.svg.append("g")
  .call(d3.axisRight(y)).attr("transform", "translate(" + this.width + ")")
  .attr("color", "green");
  

  // Add dots
  const dots = this.svg.append('g');
  dots.selectAll("dot")
  .data(this.dataCO2)
  .enter()
  .append("circle")
  .attr("cx", d => x(d.year))
  .attr("cy", d => y(d.emissionpercapita))
  .attr("r", 5)
  .style("opacity", .5)
  .style("fill", "green");

  this.svg.append("path")
  .datum(this.dataCO2)
  .attr("fill", "none")
  .attr("stroke", "green")
  .attr("stroke-width", 1.5)
  .attr("d", d3.line()
    .x(function(d) { return x(d['year']) })
    .y(function(d) { return y(d['emissionpercapita']) })
    )
  /*
  //Add labels
  dots.selectAll("text")
  .data(this.data)
  .enter()
  .attr("x", d => x(d.year))
  .attr("y", d => y(d.happinessscore))*/
}

private calcCovariance(co2data : Array<CO2Row>, hapdata : Array<HappinessRow>)
{
  let co2data_ = co2data.slice(co2data.length - hapdata.length, co2data.length)
  
  console.log(co2data_)

  const avg_1 = function(d) {
    if(d.length == 0) return 0
    return d.reduce( (a,b) => (a.emissionpercapita)  + (b.emissionpercapita) )
  }
  const avg_2 = function(d) {
    if(d.length == 0) return 0
    return d.reduce( (a,b) => a.happinessscore + b.happinessscore)
  }
  let avg_happiness = avg_2(hapdata)
  let avg_co2 = avg_1(co2data_)
  console.log("avg_happiness:" + avg_happiness)
  console.log("avg_co2:" + avg_co2)
  let val = 0
  for(let i = 0; i < hapdata.length; i++)
  {
    val += (avg_happiness - (hapdata[i].happinessscore)) * (avg_co2 - co2data_[i].emissionpercapita);  
  }
  console.log(val)
  val /= hapdata.length;
  return val;

}

}
